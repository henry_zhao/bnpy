"""
The :mod:`init` module gathers initialization procedures for model parameters
"""
import FromScratchGauss
import FromSaved, FromTruth

__all__ = ['FromScratchGauss', 'FromSaved', 'FromTruth']
